# botpress-playground

## run

### when in development

```bash
$ docker run \
    --name botpress \
    --rm \
    -p 3000:3000 \
    -v $(pwd)/data:/botpress/data \
    botpress/server:v12_22_0
```

### when in production

```bash
$ docker run \
    --name botpress \
    --rm \
    -p 3000:3000 \
    registry.gitlab.com/f4bio/botpress-playground:latest
```

## build

### login

```bash
$ docker login registry.gitlab.com
```

### build

```bash
$ docker build \
    --no-cache \
    --tag registry.gitlab.com/f4bio/botpress-playground:latest .
```

### push

```bash
$ docker push \
    registry.gitlab.com/f4bio/botpress-playground:latest
```