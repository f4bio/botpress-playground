FROM botpress/server:v12_22_0

LABEL maintainer="Fabio Tea <iam@f4b.io>"
LABEL version="0.0.1"

COPY ./data /botpress/data

WORKDIR /botpress
CMD ["./bp"]